<?php

class Pokemon{

    protected $name;
    protected $life;
    protected $attack;
    protected $defense;

    public function __construct($name, $life, $attack, $defense)
    {
        $this->name = $name;
        $this->life = $life;
        $this->attack = $attack;
        $this->defense = $defense;
    }

    public function beenAttacked(Pokemon $pokemon){
      $this->life = $this->life - ($pokemon->attack() - $this->defense);
    }

    public function attack(){
        return $this->attack;
    }

    public function status(){
        return $this->life;
    } 

    public function name(){
        return $this->name;
    } 
}

?>